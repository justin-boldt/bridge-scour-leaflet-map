// Creates a blue marker with the chevron down icon
var blueMarker = L.ExtraMarkers.icon({
	icon: 'fa-chevron-down',
	prefix: 'fa',
    markerColor: 'blue',
    shape: 'circle',
	iconColor: 'white'
});

// Creates a green marker with the chevron down icon
var greenMarker = L.ExtraMarkers.icon({
	icon: 'fa-chevron-down',
	prefix: 'fa',
    markerColor: 'green',
    shape: 'circle',
	iconColor: 'white'
});

// Creates a yellow marker with the chevron down icon
var yellowMarker = L.ExtraMarkers.icon({
	icon: 'fa-chevron-down',
	prefix: 'fa',
    markerColor: 'yellow',
    shape: 'circle',
	iconColor: 'white'
});

// Creates an orange marker with the chevron down icon
var orangeMarker = L.ExtraMarkers.icon({
	icon: 'fa-chevron-down',
	prefix: 'fa',
    markerColor: 'orange',
    shape: 'circle',
	iconColor: 'white'
});

// Creates a red marker with the chevron down icon
var redMarker = L.ExtraMarkers.icon({
	icon: 'fa-chevron-down',
	prefix: 'fa',
    markerColor: 'red',
    shape: 'circle',
	iconColor: 'white'
});

var sites = new L.LayerGroup();

L.marker([ 30.633509, -84.904320], {icon: orangeMarker}).bindPopup('Apalachicola River at I-10 near Chattahoochee, FL').addTo(sites),
L.marker([ 30.985357, -85.407340], {icon: orangeMarker}).bindPopup('Spring Creek at US-231 near Cambellton, FL').addTo(sites),
L.marker([ 43.126977, -112.513320], {icon: orangeMarker}).bindPopup('Snake River at Ferry Butte Road (W 500 S), Bingham County, ID').addTo(sites),
L.marker([ 43.376454, -112.168850], {icon: orangeMarker}).bindPopup('Snake River at Shelley West River Road (E 1275 N) near Shelley, ID').addTo(sites),
L.marker([ 40.273292, -75.415330]).bindPopup('East Branch Perkiomen Creek at Salfordville Road (SR 1017) near Harleysville, PA').addTo(sites),
L.marker([ 40.320598, -75.401590]).bindPopup('East Branch Perkiomen Creek at Camp Road (SR 1021) near Harleysville, PA').addTo(sites),
L.marker([ 40.298385, -75.456550]).bindPopup('Perkiomen Creek and Mill Race at Salford Station Road (SR 1024) near Perkiomenville, PA').addTo(sites),
L.marker([ 39.946217, -75.779070]).bindPopup('West Branch Brandywine Creek at Strasburg Road (SR 3062) near Coatesville, PA').addTo(sites),
L.marker([ 41.015822, -96.544339]).bindPopup('Rock Creek at Agnew Rd (C005500650)').addTo(sites),
L.marker([ 41.030267, -96.552680]).bindPopup('Rock Creek at Little Salt Rd (C005530425)').addTo(sites),
L.marker([ 40.676415, -96.274535]).bindPopup('Little Nemaha River at N 20th Rd (Unadilla South) (C006622115)').addTo(sites),
L.marker([ 41.027733, -96.296200]).bindPopup('Platte River at I-80 near Ashland, NE').addTo(sites),
L.marker([ 40.069385, -93.637881], {icon: orangeMarker}).bindPopup('Thompson River at MO-6 (A0906) near Trenton, MO').addTo(sites),
L.marker([ 36.115012, -89.613000], {icon: orangeMarker}).bindPopup('Mississippi River at I-155 (A1700) near Caruthersville, MO').addTo(sites),
L.marker([ 40.363203, -91.573931], {icon: orangeMarker}).bindPopup('Fox River at US-61 (A4584) near Wayland, MO').addTo(sites),
L.marker([ 40.469624, -93.280810]).bindPopup('Medicine Creek at US-136 (A6733) near Lucerne, MO').addTo(sites),
L.marker([ 39.456884, -91.047454], {icon: orangeMarker}).bindPopup('Mississippi River at US-54 (K0932) at Louisiana, MO').addTo(sites),
L.marker([ 40.112871, -94.297529], {icon: orangeMarker}).bindPopup('Grand River at Route A (P0250) near McFall, MO').addTo(sites),
L.marker([ 45.903941, -108.317920]).bindPopup('Yellowstone River at US-312 at Huntley, MT').addTo(sites),
L.marker([ 45.065208, -111.182450]).bindPopup('West Gallatin River at US-191 17 mi SE of Big Sky, MT').addTo(sites),
L.marker([ 45.544268, -112.332380]).bindPopup('Beaverhead River at MT-41 at Twin Bridges, MT').addTo(sites),
L.marker([ 45.896912, -111.596260]).bindPopup('Jefferson River at MT-2 near Three Forks, MT').addTo(sites),
L.marker([ 45.898644, -111.523840]).bindPopup('Madison River at I-90 near Three Forks, MT').addTo(sites),
L.marker([ 45.837700, -111.295440]).bindPopup('Baker Creek at MT-205 near Manhattan, MT').addTo(sites),
L.marker([ 45.825028, -111.271540]).bindPopup('Gallatin River at MT-205 near Manhattan, MT').addTo(sites),
L.marker([ 45.823689, -111.272140]).bindPopup('West Gallatin River at I-90 near Manhattan, MT').addTo(sites),
L.marker([ 38.228673, -87.984875], {icon: orangeMarker}).bindPopup('Wabash River at I-64 (097-0003/0004) near Grayville, IL').addTo(sites),
L.marker([ 47.887028, -96.269272]).bindPopup('Clearwater River at MN-32 (5921) at Red Lake Falls, MN').addTo(sites),
L.marker([ 46.947011, -91.795594]).bindPopup('Knife River at MN-61 (9339/9341) near Two Harbors, MN').addTo(sites),
L.marker([ 34.038128, -81.060087]).bindPopup('Smith Branch at S-126 (Clement Rd) at Columbia, SC').addTo(sites),
L.marker([ 33.662351, -79.836207]).bindPopup('Black River at US-52 at Kingstree, SC').addTo(sites),
L.marker([ 41.053079, -74.451145]).bindPopup('Potential sites in New Jersey').addTo(sites),
L.marker([ 41.185246, -73.529806]).bindPopup('RIPPOWAM RIVER at ROUTE 124 (917) at New Canaan, CT').addTo(sites),
L.marker([ 41.174894, -73.361552]).bindPopup('ASPETUCK RIVER at ROUTE 57 (1021) at Westport, CT').addTo(sites),
L.marker([ 41.175539, -73.361907]).bindPopup('SAUGATUCK RIVER at ROUTE 57 (1022) at Westport, CT').addTo(sites),
L.marker([ 41.799282, -72.312662]).bindPopup('WILLIMANTIC RIVER at US ROUTE 44 (1178) at Masfield, CT').addTo(sites),
L.marker([ 41.458323, -72.835823]).bindPopup('QUINNIPIAC RIVER at QUINNIPIAC STREET (3734) at Wallingford, CT').addTo(sites),
L.marker([ 41.397524, -71.841989]).bindPopup('PAWCATUCK RIVER at WHITE ROCK ROAD (4182) at Stonington, CT').addTo(sites),
L.marker([ 41.060923, -73.677542]).bindPopup('BYRAM RIVER at SHERWOOD AVENUE (5018) at Greenwich, CT').addTo(sites);

var mbAttr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
		'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		'Imagery © <a href="http://mapbox.com">Mapbox</a>',
	mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6IjZjNmRjNzk3ZmE2MTcwOTEwMGY0MzU3YjUzOWFmNWZhIn0.Y8bhBaUMqFiPrDRW9hieoQ';

var grayscale = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
	streets = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});

var map = L.map('map', {
	center: [38.0, -96.0],
	zoom: 5,
	layers: [grayscale, sites]
});

var baseLayers = {
	"Grayscale": grayscale,
	"Streets": streets
};

var overlays = {
	"Sites": sites
};

L.control.layers(baseLayers, overlays).addTo(map);
